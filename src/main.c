#include <stdio.h>
//Оскільки ми будемо коритсуватися смінними типом boolean, то нам треба підключити бібліотеку
#include <stdbool.h>
//Оскільки кількість потрібних цифр у числі не змінна, тому записую їх у константу
#define DIGIT_AMOUT 6

int digit_counter(long long n);

int ticket_getter();

bool happy_check(int first_number, int second_number);

int main() {
  //У головній функції першим кроком запрошую у користувача номер білету й записую його у змінну
  long long ticket = ticket_getter();
  //Створила масив, для чисел білету, щоб порахувати, чи є він щасливий
  int ticket_digits[DIGIT_AMOUT];
  //Створила змінну, для збереження результату программи
  bool happy;

  //Записую у массив змінну з заданим числом
  int k = 0;
  while (ticket != 0)
  {
      ticket_digits[k] = ticket % 10;
      ticket /= 10;
      k++;
  }

  //Створила змінні, які включають у себе суму 3-х перших й 3-х останніх елементів масива
  int first_three_numbers = ticket_digits[0] + ticket_digits[1] + ticket_digits[2];
  int second_three_numbers = ticket_digits[3] + ticket_digits[4] + ticket_digits[5];

  //Якщо ці змінні дорівнюють одне одній, то білет є щасливий
  happy = happy_check(first_three_numbers, second_three_numbers);

  return 0;
}

/** Створила функцію, що буде запрошувати у користувача 6-ти значне число
**/
int ticket_getter() {
  long long number = 120003; //Значення змінної можна змінювати за допомогою дебагера

  int ticket_digits_number = digit_counter(number);
  if(ticket_digits_number != DIGIT_AMOUT) {
    printf("%s\n", "Ви ввели некоректне число!");
    //Якщо число невірне, то функція визове сама себе, тобто відбудеться рекурсія
    return ticket_getter();
  } else {

    return number;
  }
}

bool happy_check(int first_number, int second_number) {
  if(first_number == second_number) {
    return true;
  } else {
    return false;
  }
}

//Створила функцію, що рахує кількість цифр у заданному числі
int digit_counter(long long n) {
  int count = 0;
  do {
    n /= 10;
    ++count;
  } while (n != 0);
  return count;
}
